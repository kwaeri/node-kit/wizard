/*-----------------------------------------------------------------------------
 * @package:    node-kit wizard
 * @author:     Richard B Winters
 * @copyright:  2014-2020 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.2.0
 *---------------------------------------------------------------------------*/
'use strict'

// INCLUDES
import * as assert from 'assert';
import { ExampleWizard } from '../src/wizard';


// DEFINES
//let wizard = new Wizard({});

type WizardAnswers =
{
    question1Answer?: boolean|string;
};


// SANITY CHECK - Makes sure our tests are working proerly
describe
(
    'sanity', function()
    {
        describe
        (
            'sanity()',
            function()
            {
                it
                (
                    'Should return true.',
                    function()
                    {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );
            }
        );
    }
);


// Service CHECK - Make sure the infrastructure is sensible
describe
(
    'SomeWizardServiceProvider:getServiceProviderSubscriptions', function()
    {
        describe
        (
            'SomeWizardServiceProvider.getServiceProviderSubsctiptions()',
            function()
            {
                it
                (
                    'Should return true.',
                    async () =>
                    {
                        let esp = new ExampleWizard();

                        let testMethodResult = esp.getServiceProviderSubscriptions();

                        return Promise.resolve(
                            assert.equal( JSON.stringify( testMethodResult ), JSON.stringify( { commands: {}, required: {}, optional: {}, subcommands: {} } ) )
                        );
                    }
                );
            }
        );
    }
);


// Service CHECK - Make sure the infrastructure is sensible
describe
(
    'SomeWizardServiceProvider:getServiceProviderSubscriptionHelpText', function()
    {
        describe
        (
            'SomeWizardServiceProvider.getServiceProviderSubscriptionHelpText()',
            function()
            {
                it
                (
                    'Should return true.',
                    async () =>
                    {
                        let esp = new ExampleWizard();

                        let testMethodResult = esp.getServiceProviderSubscriptionHelpText();

                        return Promise.resolve(
                            assert.equal( JSON.stringify( testMethodResult ), JSON.stringify(
                                    {
                                        helpText: {
                                            "command": "To use this service, read this HelpText."
                                        }
                                    }
                                )
                            )
                        );
                    }
                );
            }
        );
    }
);


// Service CHECK - Make sure the infrastructure is sensible
describe
(
    'SomeWizardServiceProvider:run', function()
    {
        describe
        (
            'SomeWizardServiceProvider.run()',
            function()
            {
                it
                (
                    'Should return true.',
                    async () =>
                    {
                        let esp = new ExampleWizard(),
                            testMethodResult = null,
                            answers: WizardAnswers;

                        if( esp )
                        {
                            console.log( 'Starting the wizard...' );

                            // Run the wizard, our test will press enter for us, resulting in a null answer that is directly returned
                            // in our promise
                            testMethodResult = await esp.run( {name: null} );

                            // Pack the answers from the wizard into our variable (we don't have any formatted promises returning, so
                            // expect null - providing this implementation in order to show how we might receive the result from the
                            // wizard when we're packing answers into a formatted promise):
                            answers = ( testMethodResult && testMethodResult.answers ) ? ( testMethodResult.answers as WizardAnswers ) : { question1Answer: null };
                        }

                        return Promise.resolve(
                            assert.equal
                            (
                                JSON.stringify( answers ),
                                JSON.stringify( { question1Answer: null } )
                            )
                        );
                    }
                );
            }
        );
    }
);


// Service CHECK - Make sure the infrastructure is sensible
describe
(
    'SomeWizardServiceProvider:setServiceEventMetadata', function()
    {
        describe
        (
            'SomeWizardServiceProvider.setServiceEventMetadata()',
            function()
            {
                it
                (
                    'Should return true.',
                    async () =>
                    {
                        let esp = new ExampleWizard();

                        esp.testEvents( ( data ) => { return Promise.resolve( assert.equal( JSON.stringify( data ), JSON.stringify( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } ) ) ) } );

                        esp.setServiceEventMetadata( { progressLevel: 50, notice: "Complete", log: "Test", logType: 0 } );
                    }
                );
            }
        );
    }
);
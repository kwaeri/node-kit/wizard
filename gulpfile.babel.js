/*-----------------------------------------------------------------------------
 * @package:    node-kit wizard
 * @author:     Richard B Winters
 * @copyright:  2014-2020 Massively Modified, Inc.
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.2.0
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import gts from 'gulp-typescript';                  // For compiling typescript, requires the typescript module also.
import sourcemaps from 'gulp-sourcemaps';
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import rename from 'gulp-rename';                   // For adding suffixes/renaming files (we use it to name minified js files)
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation


// DEFINES
let gtsProject  = gts.createProject( 'tsconfig.json' ); // A preliminary for transpiling typescript using microsoft's compiler


// PARSE COMMAND LINE ARGUMENTS:
const args =
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // Argument value:
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check if we supplied arguments specific to version bump processes:
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly:
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = { version: toVersion, key: '"version": "' };
}

// If type was provided, overwrite the options accordingly:
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = { type: byType, key: '"version": "' };
}


// Bumps the version in our projects files (called manually, or through the build-release task):
gulp.task
(
    'bump-file-versions',
    () =>
    {
        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit our package.json: 'build/node-kit/**/*.js'
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '!node_modules{,/**}',
                '!test{,/**}',
                '!build/test{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bumps the version in our package.json file:
gulp.task
(
    'bump-project-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Compiles Typescript files for development:
gulp.task
(
    'compile-typescript:development',
    () =>
    {
        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( sourcemaps.init() )
        .pipe( gtsProject() )
        .js.pipe( sourcemaps.write( '.', { includeContent: false, sourceRoot: './' } ) )
        .on( error, ( error ) => console.log( error ) )
        .pipe( gulp.dest( 'app' ) );
    }
);


// Compiles Typescript files:
gulp.task
(
    'compile-typescript',
    () =>
    {
        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .js.pipe( gulp.dest( 'build' ) );
    }
);


// Generates Typescript declaration files:
gulp.task
(
    'generate-typescript-declarations',
    () =>
    {
        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .dts.pipe( gulp.dest( 'build' ) );
    }
);


// Watches for changes in TS files:
gulp.task
(
    'watch',
    () =>
    {
        gulp.watch( '**/*.ts', ['default'] );
    }
);


// Cleans the build directory (removes it):
gulp.task
(
    'clean:build',
    ( done ) =>
    {
        let deleteList = ( !args.alt ) ? 'build' : 'build';
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();
    }
);


// Cleans the dist directory and bump all file header version strings:
gulp.task
(
    'build-release',
    gulp.series
    (
        'clean:build',
        'bump-project-version',
        'bump-file-versions',
        ( done ) =>
        {
            done();
        }
    )
);


// Runs clean processes:
gulp.task
(
    'clean',
    gulp.series
    (
        'clean:build',
        ( done ) =>
        {
            done();
        }
    )
);


// Runs default build process:
gulp.task
(
    'build:development',
    gulp.series
    (
        'clean',
        'compile-typescript:development',
        'generate-typescript-declarations',
        ( done ) =>
        {
            done();
        }
    )
);


// Runs default build process:
gulp.task
(
    'default',
    gulp.series
    (
        'clean',
        'compile-typescript',
        'generate-typescript-declarations',
        ( done ) =>
        {
            done();
        }
    )
);


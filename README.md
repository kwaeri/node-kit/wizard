# kwaeri-node-kit-wizard

[![pipeline status](https://gitlab.com/kwaeri/node-kit/wizard/badges/master/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/wizard/commits/master)  [![coverage report](https://gitlab.com/kwaeri/node-kit/wizard/badges/master/coverage.svg)](https://kwaeri.gitlab.io/node-kit/wizard/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

The @kwaeri/wizard component for the @kwaeri/node-kit application platform

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The wizard provides the interface(s) derived wizard service providers inherit when extending the base `WizardServiceProvider` in creating linear terminal/console processes to collect information that will be used in content creation (and other automated processes) for end users of @kwaeri/cli [nkm].

@kwaeri/wizard was originally included in the entry point to the platform under the directory [from root] `node-kit/core/`. In an attempt to standardize implementation - by separating concerns - as well as to ease maintainence of each component of the platform, the wizard was migrated to its own component module.

## Getting Started

**NOTE**

@kwaeri/wizard is not ready for production. We've published this module for testing and development purposes. You're free to try out anything that may already be available, but please be aware that there is likely to be many aspects of the platform which are not working and/or are completely broken. As we near completion of the new platform, we'll update documentation and provide complete examples and tutorials for getting started.

### Installation

[@kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps the various components under the kwaeri scope, and provides a single entry point to the node-kit platform for easing the process of building a kwaeri application.

However, if you wish to install @kwaeri/wizard and utilize it specifically - perform the following steps to get started:

Install @kwaeri/wizard:

```bash
npm install @kwaeri/wizard
```

### Usage

The wizard component is leveraged for deriving wizards - and their providers - which are then used by the CLI, through the Steward and Automaton, explicitly in its handling of commands passed to it by the end user - which is pretty much the core purpose of the CLI.

To leverage the wizard component, you'll first need to include it:

```typescript
// INCLUDES
import { WizardServiceProvider } from '@kwaeri/wizard';
import { ServiceProviderSubscriptionsPromise, ServiceProviderHelpTextPromise } from '@kwaeri/service';


// Example of a Wizard Service Provider
export class ExampleWizard extends WizardServiceProvider
{
    async getServiceProviderSubscriptions<T extends ServiceProviderSubscriptionsPromise>( options?: any ): Promise<T>
    {
        return Promise.resolve( <T>{ commands: {}, required: {}, optional: {}, subcommands: {} } );
    }
    async getServiceProviderSubscriptionHelpText<T extends ServiceProviderHelpTextPromise>( options?: any ): Promise<T>
    {
        return Promise.resolve( <T>{ helpText: { "command": `To use this service, read this HelpText.` } } );
    }
}

// ...
```

To be continued...but in the meantime, check out the [node-kit project wizard](https://gitlab.com/kwaeri/node-kit/node-kit-project-wizard) to see a real example of a wizard implementation.

**NOTE**

As mentioned earlier, the plan is to continue development of the myriad components of the node-kit platform - the wizard component included - and ultimately ease the process of development, maintainence, and usage of each individual component as they are decoupled from one another.


## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/groups/kwaeri/node-kit/-/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [Service Desk](mailto:incoming+kwaeri-node-kit-wizard-17966115-issue-@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/groups/kwaeri/node-kit/-/issues?scope=all&utf8=✓&state=opened&label_name[]=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
